﻿using System;

namespace Phones
{
    public class PhoneFactory
    {
        private Random _random = new Random();

        public Phone OrderPhone(DeliveryService deliveryService, string address)
        {
            var phone = new Phone();
            phone.Id = _random.Next(100000, 999999).ToString();

            return (Phone)deliveryService.Deliver(phone, address);
        }

        public PhoneX OrderPhoneX(DeliveryService deliveryService, string address)
        {
            var phone = new PhoneX();
            phone.Id = _random.Next(100000, 999999).ToString();

            return (PhoneX)deliveryService.Deliver(phone, address);
        }

    }
}
