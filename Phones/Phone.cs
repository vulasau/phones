﻿using System;
using System.Linq;

namespace Phones
{
    public class Phone: Product
    {
        protected virtual NetworkTypes[] _supportedNetworks
        {
            get { return new NetworkTypes[] { NetworkTypes.EDGE }; }
        }

        protected NetworkTypes _network;

        public NetworkTypes Network
        {
            get { return _network; }
            set
            {
                if (_supportedNetworks.Any(x => x == value))
                    _network = value;
                else
                    _network = _supportedNetworks.Last();
            }
        }

        public void Call(string number)
        {
            Console.WriteLine($"Calling {number} via {_network}");
        }

        public virtual string MakePicture(string picture)
        {
            return picture.ToLower();
        }
    }
}
