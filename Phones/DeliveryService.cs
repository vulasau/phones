﻿namespace Phones
{
    public abstract class DeliveryService
    {
        public abstract Product Deliver(Product product, string address);
    }
}
