﻿using System;

namespace Phones
{
    public class HermesDrone : DeliveryService
    {
        public override Product Deliver(Product product, string address)
        {
            Console.WriteLine($"Delivering product '{product.Id}' to {address} using drone");
            return product;
        }
    }
}
