﻿using System.Linq;

namespace Phones
{
    public class PhoneX : Phone
    {
        protected override NetworkTypes[] _supportedNetworks
        {
            get { return new NetworkTypes[] { NetworkTypes.EDGE, NetworkTypes.LTE }; }
        }

        public override string MakePicture(string picture)
        {
            var basePicture = base.MakePicture(picture);
            var pictureObjects = basePicture.Split(' ');
            var enchansedPictureObjects = pictureObjects.Select(pictureObject =>
            {
                if (pictureObject.Length < 2)
                    return pictureObject;

                var objectShape = pictureObject.First().ToString().ToUpper();
                var objectBody = new string(pictureObject.Skip(1).ToArray());

                return objectShape + objectBody;
            });

            return string.Join(" ", enchansedPictureObjects);
        }
    }
}
