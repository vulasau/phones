﻿using System;

namespace Phones
{
    class Program
    {
        static void Main(string[] args)
        {
            var hermes = new Hermes();
            var hermesDrone = new HermesDrone();
            var factory = new PhoneFactory();

            var screen = "SomE ObjectS are PhotographeD HerE";

            var phone = factory.OrderPhone(hermes, "Kaiserstr. 25, 69115, Heidelberg");
            phone.Network = NetworkTypes.LTE;
            phone.Call("+4915257811932");
            var phonePicture = phone.MakePicture(screen);
            Console.WriteLine($"'{screen}' -> '{phonePicture}'");

            Console.WriteLine();

            var phoneX = factory.OrderPhoneX(hermesDrone, "Hauptstr. 20, 69118, Heidelberg");
            phoneX.Network = NetworkTypes.LTE;
            phoneX.Call("+4915257811932");
            var phoneXPicture = phoneX.MakePicture(screen);
            Console.WriteLine($"'{screen}' -> '{phoneXPicture}'");
        }
    }
}
