﻿using System;

namespace Phones
{
    public class Hermes : DeliveryService
    {
        public override Product Deliver(Product product, string address)
        {
            Console.WriteLine($"Delivering product '{product.Id}' to {address}");
            return product;
        }
    }
}
